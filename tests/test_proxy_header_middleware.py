from typing import Tuple

from fastapi import APIRouter, FastAPI
from fastapi.testclient import TestClient

from fastapi_proxiedheadersmiddleware import ProxiedHeadersMiddleware

# These tests work by configuring a route with `/` on the end, and then making a request without
# the `/`. We expect FastAPI to redirect to the path with the `/` on the end - but to construct the
# url using the X-forwarded-fields.


def configure_client() -> Tuple[FastAPI, TestClient]:
    app = FastAPI()
    app.add_middleware(ProxiedHeadersMiddleware)
    client = TestClient(app)
    return app, client


def test_forwarded_with_prefix():
    app, client = configure_client()
    router = APIRouter()
    router.get("/{path}/")(lambda path: "OK")
    app.include_router(router)
    resp = client.get(
        "/sample", allow_redirects=False, headers={
            "X-Forwarded-Host": "test.com",
            "X-Forwarded-Prefix": "/prefix",
        }
    )
    assert resp.status_code == 307
    assert resp.headers["location"] == "http://test.com/prefix/sample/"


def test_no_forwarded():
    app, client = configure_client()
    router = APIRouter()
    router.get("/{path}/")(lambda path: "OK")
    app.include_router(router)
    resp = client.get(
        "/sample", allow_redirects=False
    )
    assert resp.status_code == 307
    assert resp.headers["location"].startswith("http://testserver/")
