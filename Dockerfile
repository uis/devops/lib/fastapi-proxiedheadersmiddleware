# This Dockerfile is just used for testing purposes and therefore builds in tox
# to the image.

FROM python:3.13-alpine

WORKDIR /usr/src/lib

ADD . .

RUN pip install tox
RUN pip install -r requirements.txt
